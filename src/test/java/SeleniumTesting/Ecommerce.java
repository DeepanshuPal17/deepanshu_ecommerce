package SeleniumTesting;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


public class Ecommerce {
    public static void main(String []args) {
        try{
            System.setProperty("webdriver.chrome.driver", "src/test/chromedriver");
            WebDriver driver = new ChromeDriver();
            driver.get("https://www.flipkart.com/");
            driver.manage().window().maximize();
            Thread.sleep(1000);
            driver.findElement(By.xpath("//button[@class='_2KpZ6l _2doB4z']")).click();
            driver.findElement(By.className("_3704LK")).sendKeys("Cosmetics");
            driver.findElement(By.xpath("//button[@class='L0Z3Pu']")).click();
            Thread.sleep(1000);
            driver.findElement(By.xpath("//a[@class='_2SqgSY']")).click();
            Thread.sleep(1000);
            driver.findElement(By.xpath("//a[@class='s1Q9rs']")).click();
            Thread.sleep(1000);
//            System.out.println("1");
            Thread.sleep(1000);
            List<String> tabs=new ArrayList<>(driver.getWindowHandles());
            driver.switchTo().window(tabs.get(tabs.size()-1));
            driver.findElement(By.className("_36yFo0")).sendKeys("110001");
            Thread.sleep(1000);
            driver.findElement(By.xpath("//span[@class='_2P_LDn']")).click();
            Thread.sleep(1000);
            driver.findElement(By.xpath("//button[@class='_2KpZ6l _2U9uOA _3v1-ww']")).click();
            Thread.sleep(1000);
            driver.findElement(By.xpath("//button[@class='_2KpZ6l _2ObVJD _3AWRsL']")).click();
            Thread.sleep(1000);
            driver.findElement(By.xpath("//input[@class='_2IX_2- _17N0em']")).sendKeys("DemoEnd");
            Thread.sleep(4000);
            driver.quit();
        }catch (Exception e){
            System.out.println(e.getClass().getName());
        }


//        driver.get("https://www.flipkart.comd/");
//        driver.findElement(By.className("_3704LK")).sendKeys("Poco F1");
//        driver.findElement(By.className("L0Z3Pu")).click();
    }
}